-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2020 at 03:04 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_java`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'salah', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL DEFAULT 'images/default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`) VALUES
(1, 'Gaming', 'Get you favorites video games, gaming consoles, and all sorts of accessories ', 'images/c1.jpg'),
(2, 'Clothing & Fashion', 'See the latest fashion products from different manufacturers at the tip of your fingers !  ', 'https://editiadedimineata.ro/wp-content/uploads/2018/07/20151008-everlane-retail-clothing.0-1170x780.jpg'),
(3, 'food', 'fetch our aisles online and choose your groceries items at the tip of your fingers with one click !', 'https://cdn.amomama.com/c4b9bc7b280f9ff4fabb7964d44ee1fc50856851551125730.jpg?width=5116&height=3355'),
(4, 'beauty', 'take care of your skin and self by buying our latest beauty products that make you look 10 years younger !', 'https://blogscdn.thehut.net/wp-content/uploads/sites/2/2019/03/04082626/323448020-CM-LF-BobbiBrownProduct-04-1200x672-Copy_1200x672_acf_cropped.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(500) NOT NULL DEFAULT 'images/default.png',
  `idcat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `image`, `idcat`) VALUES
(1, 'Redragon S101 Gaming Keyboard and Mouse Combo', 'GAMING KEYBOARD AND GAMING MOUSE COMBO: Includes Redragon RGB Backlit Gaming Keyboard and RGB Backlit Gaming Mouse. ALL-IN-ONE PC GAMER VALUE KIT, Fantastic for Gamers', 29.99, 'images/p1.jpg', 1),
(2, 'PICTEK Gaming Mouse Wired ', '【7200 DPI 5 Levels Adjustable】Pictek Programmable Gaming Mouse supports an extremely wide range of DPI from 500 to 7200. Default five DPI levels: 1200/2400/3500/5500/7200. With two DPI button', 19.99, 'https://trivaltech.com/wp-content/uploads/2020/01/PICTEK-Gaming-Mouse-1024x1024@2x.jpg', 1),
(4, 'PS4 Controller Charger', '【FAST CHARGING SYSTEM】 2-hour charging dual shock 4 controllers at the same time. No need to wait more time when charge two PS4 controllers, save your time and have more fun.\\r\\n【MULTI POWER SU】', 18.99, 'https://images-na.ssl-images-amazon.com/images/I/41iA6zlUFWL._AC_.jpg', 1),
(6, 'SWITCH CONTROLLERS NEON PINK/NEON GREEN NINTEN', 'Two Joy-Con can be used independently in each hand or together as one game controller when attached to the Joy-Con grip\\r\\nThey can also attach to the main console for use in handheld mode ', 99.96, 'https://ripleycl.imgix.net/https%3A%2F%2Fripley-prod.mirakl.net%2Fmmp%2Fmedia%2Fproduct-media%2F85203%2F20182408131641_joyconNeonPinkNeonGreen.jpg?w=750&h=555&ch=Width&auto=format&cs=strip&bg=FFFFFF&q=60&trimcolor=FFFFFF&trim=color&fit=fillmax&ixlib=js-1.1.0&s=bed2f6c3f3c48123ab918fe39c2cdb99', 1),
(7, 'ALONG FIT High Waisted Leggings with Pockets for Women ', 'COMFORTABLE FEELING: The yoga pants sewn of premium fabric glide on like butter, soft yet empowering during workouts. These high waisted yoga leggings provide you with a comfortable experience', 19.99, 'https://cdn.shopify.com/s/files/1/0058/1972/7954/products/013A6251_2048x.jpg?v=1562801331', 2),
(8, 'WearMe Pro - Reflective Lens Round Trendy Sunglasses', '100% UV400 PROTECTION - WearMe Pro\'s sunglasses come equipped with HD lenses that can filter out sunlight glare. WearMe Pro lenses protect your eyes from long term damage by blocking harmful', 22.99, 'https://img.buzzfeed.com/buzzfeed-static/static/2019-10/23/15/asset/e85e19b17c5f/sub-buzz-2921-1571843610-1.jpg?crop=425%3A451%3B0%2C0&resize=720%3A720', 2),
(9, 'Henley Shirts for Men Short Sleeve Cotton Button T-Shirts Slim Fit', 'MATERIAL: Picking premium cotton 1x1 baby rib to make this ultra soft henly shirt. Cozy,durable and with good elasticity.95%cotton 5%spandex\\r\\nWASH: Machine wash cold softly, trumble dry in lo', 19.99, 'https://belk.scene7.com/is/image/Belk?layer=0&src=3201602_710660829001_A_400_T10L00&layer=comp&$DWP_PRODUCT_PDP_MOBILE_L$', 2),
(10, 'Mrs. Butterworths Syrup, No Sugar Added, 710ml', 'Mrs. Butterworths Sugar Free Syrup contains 80% fewer calories than original syrup \\r\\n0g of sugar per serving so you can enjoy your pancakes guilt free\\r\\nDelciously thick and rich with a butt', 2.49, 'https://cdn.shopify.com/s/files/1/1317/7129/products/MapleSyrup_1400x.jpg?v=1551037327', 3),
(11, 'Makeup Brush Cleaner Dryer Electric Automatic ', 'POWERFUL MOTOR : 360 degree rotates 7700rpm to clean and dry your makeup brushes thoroughly within 10s. This makeup brush cleaner works fast and saves your time effectively. QUALITY MATE', 29.89, 'https://images-na.ssl-images-amazon.com/images/I/71fidnChwSL._SL1200_.jpg', 4),
(12, 'Makeup Brush Cleaner Dryer Electric', '[ POWERFUL MOTOR ]: 360 degree rotates 7700rpm to clean and dry your makeup brushes thoroughly within 10s. This makeup brush cleaner works fast and saves your time effectively.[ QUALITY MATER', 22.99, 'https://m.media-amazon.com/images/I/81mquDeBErL._AC_SS350_.jpg', 4),
(13, 'Hand Sanitizer with Aloe (Clear Gel), 825ml', 'New improved formula! Contains 70% Ethyl alcohol\\r\\nEfficient and easy to clean hands when soap & water are not available\\r\\nKills microorganisms upon contact and keeps the hands clean\\r\\nEnriched ', 19.96, 'https://cdn.publish0x.com/prod/fs/cachedimages/1011360600-c1998433fa6ed2440e1b32dbdd5e8de29b04ccd997df3b9e36efa20280c66938.jpeg', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_cat` (`idcat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `product_cat` FOREIGN KEY (`idcat`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
