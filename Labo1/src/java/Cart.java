
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rayow
 */
public class Cart {
    public int totalQty = 0;
    public double totalPrice = 0;
    private ArrayList<ProduitPanier> cartlist = new ArrayList<>();
    public Cart()
    {
    }
    public Cart(Cart c)
    {
        if(c!=null)
        {
            totalQty = c.totalQty;
            totalPrice = c.totalPrice;
            cartlist = c.cartlist;
        }
    }

    public void add(Produit p,int id) throws ClassNotFoundException
    {
        ProduitPanier storedItem= new ProduitPanier(p,0,p.getPrix());
        boolean exist=false;
        if(!this.cartlist.isEmpty())
        {
            for(ProduitPanier pp : this.cartlist)
            {
                if(pp.getP().getId()==id)
                {
                    storedItem = pp;
                    exist=true;
                }
            }
        }
        storedItem.setQte(storedItem.getQte()+1);
        float prix = storedItem.getPrix()*storedItem.getQte();
        storedItem.setPrix(prix);
        if(exist)
        {
            this.cartlist.set(id-1,storedItem);
        }
        else
        {
             this.cartlist.add(storedItem);
        }  
            this.totalQty++;
            this.totalPrice+=p.getPrix();
    }

    public double getTaxValue()
    {
        return totalPrice*(0.15);
    }
     public int getTotalQty()
    {
        return totalQty;
    }
    public double getTotalPrice()
    {
        double total=totalPrice+getTaxValue();
        return total;
    }
    public void addOne(Produit product)
    {
       for(ProduitPanier pp : this.cartlist)
            {
                if(pp.getP().getId()==product.getId())
                {
                    pp.setQte(pp.getQte()+1);
                }
            }
       this.totalPrice+=product.getPrix();
       this.totalQty++;
    }
    public void removeOne(Produit product)
    {
        for(ProduitPanier pp : this.cartlist)
            {
                if(pp.getP().getId()==product.getId())
                {
                    if(pp.getQte()>1)
                    {
                        pp.setQte(pp.getQte()-1);
                        this.totalPrice-=product.getPrix();
                        this.totalQty--;
                    }
                    else
                    {
                        remove(product);
                    }
                }
            }
       
    }
    public void remove(Produit product)
    {
        ProduitPanier ppt=new ProduitPanier();
        for(ProduitPanier pp : this.cartlist)
            {
                if(pp.getP().getId()==product.getId())
                {
                    ppt=pp;
                }
            }
        this.totalPrice-=ppt.getP().getPrix()*ppt.getQte();
        this.totalQty-=ppt.getQte();
        cartlist.remove(ppt);
    }
    public ArrayList<ProduitPanier> getCartList()
    {
        return cartlist;
    }
}
