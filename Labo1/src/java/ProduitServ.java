import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rayow
 */
public class ProduitServ extends HttpServlet {
    private String returnedVal="",returnedVal1="",returnedVal2="",id_cat="",edit="";
    private  List<Categorie> cl = new ArrayList<>();
    private HttpSession session;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         this.id_cat = request.getParameter("id_cat");
         session = request.getSession(); 
         getProduits();
         getCatInfo();
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
             returnedVal=returnedVal1+returnedVal2;
            out.println(returnedVal);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public void getProduits()
    {
        try {
            produitGate c= new produitGate("jdbc:mysql://localhost/project_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","",id_cat);
            List<Produit> l = new ArrayList<>();
            l=c.getAllProduitsInfo();
            String val="<div class='row py-4'><div class='col-sm-5 bg-white py-2 col-xs-12 rounded justify-content-center ' >";
            if(session.getAttribute("username")!=null && session.getAttribute("password")!=null)
                {
                    categorieGate cg = new categorieGate("jdbc:mysql://localhost/project_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
                    cl=cg.getAllCategoriesInfo();
                    String options="";
                    for (Categorie element : cl) 
                    {
                        options += "  <option value=\""+element.getId()+"\">"+element.getNom()+"</option>";
                    } 
                    val+= "<i class='fas fa-2x text-primary fa-plus-square btn' data-toggle=\"collapse\" data-target=\"#collapseExample2\" aria-expanded=\"false\" aria-controls=\"collapseExample2\"> Add Product</i>"+
                             "<div class=\"collapse\" id=\"collapseExample2\">\n"+
                             " <div class=\"card card-body text-center\">\n" +
                            "<form action='AddProductServ'><h3>Category</h3><select name='IdCat'>";
                    val+=options;
                    val+= "</option></select><h3>Name</h3><input type='text' class='m-2 w-100' name='Name' value='' placeholder='Name of Product'>\n"+
                    "<h3>Price</h3><input type='number' name='Price' value='20'>$<h3>Description</h3><br/><textarea class='m-2 w-100' name='Desc' value=''> </textarea><br/><button type='submit' class='btn btn-outline-success m-2'>Save</button></form> </div>\n" +
                    "</div>";
                }
            for (Produit element : l) 
            {
                val+="<div class='col-12 p-0 my-4 bg-light shadow getProductInfo rounded' data-prodid="+element.getId()+">\n" +
        "<div class='product-grid w-100 mh-100 p-0 m-0'>\n" +
        "<div class='product-image h-auto w-100'>\n" +
        "<a class='row mh-100'>\n" +
        "<img class='col-6  m-auto h-100'  src='"+element.getImg()+"'>\n" +
        "<div class='col-6' style='position: relative;'><h4 class=\" \" style='word-break: break-all;'>"+element.getNom()+"</h4>\n"+    
        "<big class='text-success '>"+element.getPrix()+" $</big>\n"+        
        "<br/></div>\n" +        
        "</a>\n" ;
        
               if(session.getAttribute("username")!=null && session.getAttribute("password")!=null)
                { 
                    val+="<p><form action='RemoveProductServ' style='position: absolute;bottom:0;right:0;'><input type='hidden' value='"+element.getId()+"' name='Id'><button type='submit' class='btn btn-outline-danger border-0 mx-3 p-1 float-left' ><i class='fa fa-trash' style='font-size:1.4em;'></i></button></form></p>";
                }
               else
               {
                   val+="<p><button class='btn btn-outline-success border-0 mx-3 p-1 float-right' onclick='add_to_cart("+element.getId()+")' style='position: absolute;bottom:0;right:0;'><i class=\"fas fa-2x fa-cart-plus p-0\"></i></button></p>";
               }
         val+="</div>\n" +
        "</div>\n" +
        "</div>";
            } 
            this.returnedVal2=val+"</div><div class='col-sm-7 col-xs-12 infos'></div></div>";
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CatServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void getCatInfo()
    {
        try {
            categorieGate cg= new categorieGate("jdbc:mysql://localhost/project_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","");
            Categorie c = new Categorie();
            c=cg.getCategorieInfo(id_cat);
           
               String val = "";
               if(session.getAttribute("username")!=null && session.getAttribute("password")!=null)
               {
                    val += "<div class=' col-12 p-2 m-0 w-auto'><i class='fas fa-2x text-primary fa-plus-square bg-white rounded-pill  btn' data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\"> Add Category</i>"
                            + "<div class=\"collapse\" id=\"collapseExample\">\n" +
                            "  <div class=\"card card-body text-center\">\n" +
                            "<form action='AddCategoryServ'><h3>Title</h3><input type='text' class='m-2 w-50' name='Name' value='' placeholder='Name of Category'>"+
                            "<h3>Description</h3><br/><textarea class='m-2 w-50' name='Desc' value=''> </textarea><br/><button type='submit' class='btn btn-outline-success m-2'>Save</button> </form></div>\n" +
                            "</div></div>";
               }
                       
                       
                val+="<div class=\"row bg-white p-5 text-center\" style='position:relative;'>\n" ;
                if(session.getAttribute("username")!=null && session.getAttribute("password")!=null)
                {
                    val+="<div class='col-sm-8 col-xs-6'>\n" +
                "<form action='EditCategoryServ' method='post'>\n"+
                "<input type='text' name='Name' class='display-4 w-100' value='"+c.getNom()+"'>\n" +
                "<p><textarea type='text' name='Desc' class='lead my-4 w-100' value='" +c.getDesc() +"'>"+c.getDesc()+"</textarea></p>\n" +
                "<input type='hidden' name='Id' value='"+c.getId()+"'><button class='btn btn-outline-success float-left m-1' type='submit' >\n" +
                "<i class='fa fa-check' style='font-size:1.4em;'></i>Apply changes</button></form></div>\n" +
               "<div class='col-sm-4 col-xs-6'>\n" +
                "<form action='CategoryFileUploadHandler' method='post' enctype='multipart/form-data'>\n" +  
                "<input type='hidden' name='Id' value='"+c.getId()+"'><input type='file' name='file' />\n" +
                "<input type='submit' value='upload' />\n" +
                "</form><img src="+c.getImg()+" alt='Category image' class='img-thumbnail'>\n" +
                "</div>\n"+
                "<form action='RemoveCategoryServ' style='position: absolute;top:0;left:0;'><button class='btn btn-outline-danger float-right m-1' type='submit'>\n" +
                 "<i class='fa fa-trash' style='font-size:1.6em;'></i></button><input type='hidden' value='"+c.getId()+"' name='Id'></form></div>" +
                "</div>\n";
                }
               else{val+= "<div class=\"col-sm-8 col-xs-6 catnonedit\">\n" +
                "<h2 class=\"display-4 \">"+c.getNom()+"</h2>\n" +
                "<p class=\"lead mb-0\">\n" +
                c.getDesc() +
                "</p>\n" +
                "</div>\n" +
                "<div class=\"col-sm-4 col-xs-6\">\n" +
                "<img src="+c.getImg()+" alt=\"Category image\" class=\"img-thumbnail\">\n" +
                "</div>\n" +             
                "</div>\n";}
                this.returnedVal1=val;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CatServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
