/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rayow
 */
public class ProduitPanier {
    private Produit P;
    private int Qte;
    private float Prix;
    public ProduitPanier()
    {
        this.P=new Produit();
        this.Qte=0;
        this.Prix=0;
    }
    public ProduitPanier(Produit P,int Qte,float Prix)
    {
        this.P=P;
        this.Qte=Qte;
        this.Prix=Prix;
    }
    public Produit getP()
    {
        return this.P;
    }
    public void setP(Produit P)
    {
        this.P=P;  
    }
     public int getQte()
    {
        return this.Qte;
    }
    public void setQte(int Qte)
    {
        this.Qte=Qte;  
    }
     public float getPrix()
    {
        return this.Prix;
    }
    public void setPrix(float Prix)
    {
        this.Prix=Prix;  
    }
}
