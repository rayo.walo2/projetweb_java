/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

/**
 *
 * @author rayow
 */
public class admin {
    private int Id;
    private String Username,Password;
     public admin()
    {
        this.Id=0;
        this.Username="";
        this.Password="";
    }
    public admin(int Id,String Username,String Password)
    {
        this.Id=Id;
        this.Username=Username;
        this.Password=Password;
    }
    public int getId()
    {
        return this.Id;
    }
    public void setId(int Id)
    {
        this.Id=Id;  
    }
    public String getUsername()
    {
        return this.Username;
    }
    public void setUsername(String Username)
    {
        this.Username=Username;  
    }
    public String getPassword()
    {
        return this.Password;
    }
    public void setPassword(String Password)
    {
        this.Password=Password;  
    }
}
