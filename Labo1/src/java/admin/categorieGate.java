package admin;


import java.sql.Connection;
import java.sql.*; 
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class categorieGate {
    private Connection conn;
    private PreparedStatement allCategoriesInfo,CategorieInfo,addCategory,editCategory,removeCategory,editCategoryImg;
    private ResultSet result;
    private String url,user,pw;
    
    //constructeur de la class 
    public categorieGate  (String url,String user,String pw) throws ClassNotFoundException
       {         
        this.url = url;
        this.user = user;
        this.pw = pw;
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allCategoriesInfo = conn.prepareStatement("select * from categories;");
            CategorieInfo = conn.prepareStatement("select * from categories where id=?;");
            addCategory = conn.prepareStatement("insert into categories(name,description) values (?,?);");
            removeCategory = conn.prepareStatement("delete from categories where id=?;");
            editCategory = conn.prepareStatement("update categories set name=?,description=? where id=?;");
            editCategoryImg= conn.prepareStatement("update categories set image=? where id=?;");
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
    

    public List<Categorie> getAllCategoriesInfo()
    {
        List<Categorie> listOfCategories = new ArrayList<>();
        try 
        {
            result = this.allCategoriesInfo.executeQuery();
            while(result.next())
            {
                listOfCategories.add(new Categorie(result.getInt("id"),result.getString("name"),
                                result.getString("description"),result.getString("image")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return listOfCategories;
    }
    public Categorie getCategorieInfo(String idCat)
    { 
        try 
        {
            CategorieInfo.setString(1, idCat);
            result = this.CategorieInfo.executeQuery();
            while(result.next())
            {
                return(new Categorie(result.getInt("id"),result.getString("name"),
                                result.getString("description"),result.getString("image")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }
    public void addCategory(Categorie c) {
        try 
        {
            addCategory.setString(1, c.getNom());
            addCategory.setString(2, c.getDesc());
            
            int result = this.addCategory.executeUpdate();    
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void removeCategory(Categorie c) {
        try 
        {
            removeCategory.setString(1, Integer.toString(c.getId()));
            int result = this.removeCategory.executeUpdate();    
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editCategory(Categorie c) {
        try 
        {
            editCategory.setString(1, c.getNom());
            editCategory.setString(2, c.getDesc());
            editCategory.setInt(3, c.getId());
            
            int result = this.editCategory.executeUpdate();    
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    void editCategoryImg(Categorie c) {
         try 
        {
            editCategoryImg.setString(1, c.getImg());
            editCategoryImg.setInt(2, c.getId());
            
            int result = this.editCategoryImg.executeUpdate();    
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}