package admin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rayow
 */
public class Categorie {
    private String Nom,Desc,Img;
    private int Id;
    public Categorie()
    {
        this.Id=0;
        this.Nom="";
        this.Desc="";
        this.Img="";
    }
    public Categorie(int Id,String Nom,String Desc,String Img)
    {
        this.Id=Id;
        this.Nom=Nom;
        this.Desc=Desc;
        this.Img=Img;
    }
    public int getId()
    {
        return this.Id;
    }
    public void setId(int Id)
    {
        this.Id=Id;  
    }
    public String getNom()
    {
        return this.Nom;
    }
    public void setNom(String Nom)
    {
        this.Nom=Nom;  
    }
    public String getDesc()
    {
        return this.Desc;
    }
    public void setDesc(String Desc)
    {
        this.Desc=Desc;  
    }
    public String getImg()
    {
        return this.Img;
    }
    public void setImg(String Img)
    {
        this.Img=Img;  
    }
}
