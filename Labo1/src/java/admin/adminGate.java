/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rayow
 */
public class adminGate {
     private Connection conn;
    private PreparedStatement AllAdminsInfo,AdminInfo;
    private ResultSet result;
    private String url,user,pw;
    
    //constructeur de la class 
    public adminGate  (String url,String user,String pw) throws ClassNotFoundException
       {         
        this.url = url;
        this.user = user;
        this.pw = pw;
        
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            AllAdminsInfo = conn.prepareStatement("select * from admin;");
            AdminInfo = conn.prepareStatement("select * from admin where username=? and password=?;");
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(adminGate.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
    

    public List<admin> getAllAdminsInfo()
    {
        List<admin> listOfAdmins = new ArrayList<>();
        try 
        {
            result = this.AllAdminsInfo.executeQuery();
            while(result.next())
            {
                listOfAdmins.add(new admin(result.getInt("id"),result.getString("username"),
                                result.getString("password")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(adminGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return listOfAdmins;
    }
    public admin getAdminInfo(String username,String password)
    { 
        try 
        {
            AdminInfo.setString(1, username);
            AdminInfo.setString(2, password);
            result = this.AdminInfo.executeQuery();
            while(result.next())
            {
                return(new admin(result.getInt("id"),result.getString("username"),
                                result.getString("password")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(adminGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }
}
