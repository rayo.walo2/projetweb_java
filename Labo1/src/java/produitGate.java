import java.sql.Connection;
import java.sql.*; 
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class produitGate {
    private Connection conn;
    private PreparedStatement allProduitsInfo,ProduitInfo;
    private ResultSet result;
    private String idCat;
    private String url,user,pw;
    
    //constructeur de la class 
    public produitGate  (String url,String user,String pw,String idCat) throws ClassNotFoundException
       {         
        this.url = url;
        this.user = user;
        this.pw = pw;
        this.idCat=idCat;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(this.url, this.user, this.pw);
            allProduitsInfo = conn.prepareStatement("select * from products where idCat=?;");
            ProduitInfo = conn.prepareStatement("select * from products where id=?;");
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
       }
    

    public List<Produit> getAllProduitsInfo()
    {
        List<Produit> listOfProduits = new ArrayList<>();
        try 
        {
            allProduitsInfo.setString(1, idCat);
            result = this.allProduitsInfo.executeQuery();
            while(result.next())
            {
                listOfProduits.add(new Produit(result.getInt("id"),result.getString("name"),result.getFloat("price"),
                                 result.getString("description"),result.getString("image")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return listOfProduits;
    }
    public Produit getProduitInfo(String idProd)
    { 
        try 
        {
            ProduitInfo.setString(1, idProd);
            result = this.ProduitInfo.executeQuery();
            while(result.next())
            {
                return(new Produit(result.getInt("id"),result.getString("name"),result.getFloat("price"),
                                 result.getString("description"),result.getString("image")));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(categorieGate.class.getName()).log(Level.SEVERE, null, ex);
        }
         return null;
    }
}
