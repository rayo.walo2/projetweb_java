/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rayow
 */
public class Produit {
    private String Nom,Desc,Img;
    private int Id;
    private float Prix,Shipping,Handling;
    public Produit()
    {
        this.Id=0;
        this.Nom="";
        this.Prix=0;
        this.Shipping=0;
        this.Handling=0;
        this.Desc="";
        this.Img="";
    }
    public Produit(int Id,String Nom,float Prix,String Desc,String Img)
    {
        this.Id=Id;
        this.Nom=Nom;
        this.Prix=Prix;
        this.Shipping=0;
        this.Handling=0;
        this.Desc=Desc;
        this.Img=Img;
    }
    public int getId()
    {
        return this.Id;
    }
    public void setId(int Id)
    {
        this.Id=Id;  
    }
    public String getNom()
    {
        return this.Nom;
    }
    public void setNom(String Nom)
    {
        this.Nom=Nom;  
    }
    public String getDesc()
    {
        return this.Desc;
    }
    public void setDesc(String Desc)
    {
        this.Desc=Desc;  
    }
    public String getImg()
    {
        return this.Img;
    }
    public void setImg(String Img)
    {
        this.Img=Img;  
    }
    public float getPrix()
    {
        return this.Prix;
    }
    public void setPrix(float Prix)
    {
        this.Prix=Prix;  
    }
    public float getShipping()
    {
        return this.Shipping;
    }
    public void setShipping(float Shipping)
    {
        this.Shipping=Shipping;  
    }
    public float getHandling()
    {
        return this.Handling;
    }
    public void setHandling(float Handling)
    {
        this.Handling=Handling;  
    }
}
