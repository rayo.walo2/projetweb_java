/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rayow
 */
@WebServlet(urlPatterns = {"/ShoppingCatServ"})
public class ShoppingCatServ extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String output,paypal;
        DecimalFormat df = new DecimalFormat("0.00");
        Cart c =(Cart) session.getAttribute("cart");
        session.setAttribute("cartQty",0);
        if(c==null || c.totalQty==0)
        {
            
            output="    <div class='text-muted text-center py-5 m-auto' style='word-wrap: break-word;font-size:1.3em'><big class='p-0 m-0'>Your shopping cart is <b>empty</b>.</big><br><br><i>Come back later!</i></div>";
        }
        else
        {
            session.setAttribute("cartQty",c.totalQty);
            paypal="<form target='paypal' action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'>"+
                    "<input type='hidden' name='cmd' value='_cart'>"+
                    "<input type='hidden' name='upload' value='1'>"+
                    "<input type='hidden' name='business' value='rayotest@business.personal'>";
            
            output="<div class='row rounded shadow-sm mb-5'><div class='col-lg-12 p-2 bg-white '>"+
                    "<div onclick='removeAll()' class='m-auto btn btn-white text-danger border-0 rounded' style='font-size:1.3em;cursor:pointer;'><i class='fa fa-times float-left'>Clear Cart</i></div></div>"+
                    "<div class='col-lg-12 p-5 bg-white '><div class='table-responsive cart-table'><table class='table'><thead><tr>"+
                    "<th scope='col' class='border-0 bg-light'><div class='p-2 px-3 text-uppercase'>Product</div></th>"+
                    "<th scope='col' class='border-0 bg-light'><div class='py-2 text-uppercase'>Price</div></th>"+
                    "<th scope='col' class='border-0 bg-light'><div class='py-2 text-uppercase'>Quantity</div></th>"+
                    "<th scope='col' class='border-0 bg-light'><div class='py-2 text-uppercase'>Remove</div> </th></tr></thead><tbody>";
            
            for(int i = 0; i < c.getCartList().size(); i++)
            {
                int k=i+1;
                output += "<tr><th scope='row' > <div class='p-2 prod-img'>"+
                        "<img src='"+c.getCartList().get(i).getP().getImg()+"' alt='' width='70' class='img-fluid rounded shadow-sm'>"+
                        "<div class='ml-3 d-inline-block align-middle'><h5 class='mb-0'> "+
                        "<a href='' class='text-dark d-inline-block align-middle'></a></h5><span class='text-muted font-weight-normal font-italic d-block'>"+c.getCartList().get(i).getP().getNom()+"</span>"+
                        "</div></div></th> <td class='align-middle'><strong class='text-success'>"+c.getCartList().get(i).getP().getPrix()+"$</strong></td>"+
                        "<td class='align-middle'><div class='number-input md-number-input row'><div class='col-lg-6 col-md-12 col-sm-12 col-12 p-2'>"+
                        "<input id='quantity' min='1' name='quantity' value='"+c.getCartList().get(i).getQte()+"' type='number' disabled></div>"+
                        "<div class='col-lg-6 col-md-12 col-sm-12 col-12 p-2'>"+
                        "<i onclick='addOne("+c.getCartList().get(i).getP().getId()+")' class='btn btn-success text-white fas fa-plus plus-one'></i>"+
                        "<i onclick='removeOne("+c.getCartList().get(i).getP().getId()+")' class='btn btn-danger text-white fas fa-minus minus-one'></i></div></div></td>"+
                        "<td class='align-middle'><button onclick='remove("+c.getCartList().get(i).getP().getId()+")' class='btn btn-white text-danger'><i class='fa fa-trash' style='font-size:1.4em;'></i></button></td></tr>";
                paypal+="<input type='hidden' name='item_name_"+k+"' value='"+c.getCartList().get(i).getP().getNom()+"'>"+
                        "<input type='hidden' name='amount_"+k+"' value='"+c.getCartList().get(i).getP().getPrix()*1.15+"'>"+
                        "<input type='hidden' name='quantity_"+k+"' value='"+c.getCartList().get(i).getQte()+"'>"+
                        "<input type='hidden' name='shipping_"+k+"' value='"+c.getCartList().get(i).getP().getShipping()+"'>"+
                        "<input type='hidden' name='handling_"+k+"' value='"+c.getCartList().get(i).getP().getHandling()+"'>";
            }
            
            output+="</tbody></table></div></div></div><div class='row py-5 p-4 bg-white rounded shadow-sm'><div class='col-lg-6'><div class='bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold'>Coupon code</div> <div class='p-2'>"+
                    "<p class='font-italic mb-4'>If you have a coupon code, please enter it in the box below</p>"+
                    "<div class='input-group row p-0 mb-4 border rounded-pill p-2'>"+
                    " <div class='col-lg-1 col-md-1 col-sm-1 col-1 py-1'><span class='text-muted p-0 code-returned'></span></div>"+
                    " <div class='col-lg-7 col-md-7 col-sm-10 col-10'><input type='text' placeholder='Apply coupon' class='form-control border-0 coupon-code' required></div>"+
                    "<div class='input-group-append border-0 col-lg-4 col-md-4 col-sm-12 col-12  p-0'>"+
                    " <button type='button' onclick='applyCoupon()' class='btn btn-dark float-right w-100 rounded-pill coupon-btn' style='z-index:1;'><i class='fa fa-gift mr-2'></i>Apply coupon</button>"+
                    " </div> </div></div> <div class='bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold'>Instructions for our employees</div>"+
                    " <div class='p-4'><p class='font-italic mb-4'>If you have some information for the employees you can leave them in the box below</p><textarea name='msg' cols='30' rows='2' class='form-control'></textarea>   </div>  </div> <div class='col-lg-6'>"+
                    "<div class='bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold'>Order summary</div><div class='p-4'>"+
                    "<p class='font-italic mb-4'>Coupons and additional discounts are shown below.You can only apply one at a time.</p>"+
                    "<ul class='list-unstyled mb-4'>"+
                    "<li class='d-flex justify-content-between py-3 border-bottom'><strong class='text-muted'>Order Subtotal</strong><strong class='text-dark subtotal-price'>"+df.format(c.totalPrice)+"$</strong></li>"+
                    "<li class='d-flex justify-content-between py-3 border-bottom'><strong class='text-muted'>Coupons & discounts</strong><strong class='text-danger coupon-price'>-$</strong></li>"+
                    "<li class='d-flex justify-content-between py-3 border-bottom'><strong class='text-muted'>tax</strong><strong  class='text-muted tax-price'>+"+df.format(c.getTaxValue())+"$</strong></li>"+
                    "<li class='d-flex justify-content-between py-3 border-bottom'><strong class='text-muted'>Total</strong><h5 class='font-weight-bold text-success total-price'>"+df.format(c.getTotalPrice())+"$</h5> </li>"+
                    "</ul>";
            
            paypal+="<input type='hidden' name='currency_code' value='CAD'>"+
                    "<input type='hidden' name='return' value='https://www.facebook.com/play.rapboy'>"+
                    "<input type='hidden' name='cancel_return' value='https://www.instagram.com/salah.edd_/'>"+
                    "<input type='submit' name='submit' value='checkout' class='btn btn-danger rounded-pill py-2 btn-block'></form>";
            output+=paypal;
            output+="</div></div></div>";
        }
        try (PrintWriter out = response.getWriter()) {
            out.println(output);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
