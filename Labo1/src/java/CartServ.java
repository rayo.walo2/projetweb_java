/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rayow
 */
@WebServlet(urlPatterns = {"/addCart"})
public class CartServ extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Cart c;
        if(session.getAttribute("cart")!=null)
        {
           c = new Cart((Cart)session.getAttribute("cart"));
        }
        else
        {
            c = new Cart();
        }
        String id_prod = request.getParameter("id_prod");
        produitGate g= new produitGate("jdbc:mysql://localhost/project_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","","");
        Produit p = new Produit();
        p=g.getProduitInfo(id_prod);
        c.add(p,p.getId());
        session.setAttribute("cart", c);
        session.setAttribute("cartQty", c.totalQty);
        Cookie qtyCookie = new Cookie("cartQty", Integer.toString(c.totalQty));
        response.addCookie(qtyCookie);
        String output= "<div class='row' onclick=\"window.location.href='cart.jsp'\" style='cursor:pointer;'><div class='imagesearch col-3 text-center'>\n"+
        "<img src='"+p.getImg()+"' class='rounded-circle notif-img' width='60' height='50' alt=''></div><div class='col-6 Product text-left'>\n"+
        "<strong> "+p.getNom().substring(0, 20)+"...</strong></div><div class='col-3'><i class='fa fa-3x fa-plus-circle'></i></div></div>";
         try (PrintWriter out = response.getWriter()) {
            out.println(output);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CartServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CartServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
