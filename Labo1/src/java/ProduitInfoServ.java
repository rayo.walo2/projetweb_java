/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rayow
 */
@WebServlet(urlPatterns = {"/getProduitInfo"})
public class ProduitInfoServ extends HttpServlet {
    private HttpSession session;

    private String returnedVal,id_prod="",val ;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        this.id_prod = request.getParameter("id_prod");
        session = request.getSession();
         getProduitInfo();
        try (PrintWriter out = response.getWriter()) {
            out.println(returnedVal);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProduitInfoServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProduitInfoServ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    public void getProduitInfo() throws ClassNotFoundException
    {   
        produitGate c= new produitGate("jdbc:mysql://localhost/project_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","","");
        Produit p = new Produit();
        p=c.getProduitInfo(id_prod);
        if(session.getAttribute("username")!=null && session.getAttribute("password")!=null)
        {
        val="<div class='col-sm-12 col-xs-12 bg-white py-4'style='position:relative;'>"+
        "<div class=\"row py-4\">\n" +
        "<div class=\"col-sm-12 col-xs-12 text-center  py-2\">\n" +
        "<form action=\"FileUploadHandler\" method=\"post\" enctype=\"multipart/form-data\">\n" +  
        "<input type='hidden' name='Id' value='"+p.getId()+"'><input type=\"file\" name=\"file\" />\n" +
        "<input type=\"submit\" value=\"upload\" />\n" +
        "</form><img src="+p.getImg()+" alt=\"Category image\" class=\"img-thumbnail w-50 m-auto \">\n" +
        "</div>\n" +
        "<div class=\"col-sm-12 col-xs-12\" >\n" +
        "<form action='EditProductServ'>"+
        "<h2 class=\"display-4 \"><textarea type='text' name='Name' class='w-100'>"+p.getNom()+"</textarea></h2>\n" +
        "<p class=\"lead  mb-3\"><textarea type='text' name='Desc' class='w-100' rows='4'>\n" +
        p.getDesc() +
        "</textarea></p>\n" +
        "<h3 class='text-primary'><input type='text' name='Price' value='"+p.getPrix()+"'> $</h3>"+
        "<input type='hidden' value='"+p.getId()+"' name='Id'><p><button type='submit' class='btn btn-outline-success float-left m-1' style='position: absolute;bottom:-1;right:100;'>\n"+
        "<i class='fa fa-check' style='font-size:1.4em;'></i>Apply changes</button></div></p>\n" +               
        "</form><form action='RemoveProductServ' style='position: absolute;top:0;right:0;'><button type='input' class='btn btn-outline-danger float-right m-1' >\n"+
        "<i class='fa fa-trash' style='font-size:1.8em;'></i></button><input type='hidden' value='"+p.getId()+"' name='Id'></form></div>\n";

        }
        else
        {
            val="<div class='col-sm-12 col-xs-12 bg-white py-2'>"+
            "<div class=\"row\">\n" +
            "<div class=\"col-sm-12 col-xs-12 text-center  py-2\">\n" +
            "<img src="+p.getImg()+" alt=\"Category image\" class=\"img-thumbnail w-50 m-auto \">\n" +
            "</div>\n" +
            "<div class=\"col-sm-12 col-xs-12\" style='position:relative;'>\n" +

            "<h2 class=\"display-4 \">"+p.getNom()+"</h2>\n" +
            "<p class=\"lead  mb-3\">\n" +
            p.getDesc() +
            "</p>\n" +
            "<h3 class='text-primary'>"+p.getPrix()+" $</h3>"+
            "<p><button class='btn btn-outline-success float-right m-3' onclick='add_to_cart("+p.getId()+")' style='position: absolute;bottom:0;right:0;'>\n"+
            "<i class='fa fa-shopping-cart'> </i>Add to cart</button></div></p>\n" +               
            "</div>\n";
        }
        this.returnedVal=val+"</div>";
    }
}
