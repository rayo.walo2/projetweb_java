<%-- 
    Document   : index_admin
    Created on : 2020-04-23, 05:11:00
    Author     : rayow
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
String username=(String)session.getAttribute("username");
String password=(String)session.getAttribute("password");
%>
<!DOCTYPE html>
<%
    if(username==null && password==null)
    { 
        String redirectURL = "index.jsp";
        response.sendRedirect(redirectURL);
    }
%>
<html>	
	<head>
		
		<title>UniScript</title>
	    <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
            <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <link rel="stylesheet" href="style.css"/>
            <style></style>
    </head>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">
		
		<div class="vertical-nav active" id="sidebar">
            <div class="row">
				<div class="col-9">
				    <div class="btn-group float-left my-4 mx-3">
				        <button type="button" class="btn btn-white btn-outline-dark active lightbtn px-2" onclick="activate_light_theme();"><i class="fas fa-sun px-2" style="font-size:1.3em;" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-white btn-outline-dark darkbtn px-2" onclick="activate_dark_theme();"><i class="fas fa-moon px-2" style="font-size:1.3em;" aria-hidden="true"></i></button>
						
					</div>
					
				</div>
				<div class="col-9">
					<div style="float:left;margin-left:150px;position:absolute;top:-60px">
				<button  class="btn btn btn-outline-secondary"><a  style="text-decoration:none" href="locale/fr">FR</a></button>
				<button  class="btn  btn btn-outline-secondary"><a style="text-decoration:none"  href="locale/en">EN</a></button></div>
                                </div>
                                <button id="sidebarCollapse2" class="col-2 btn btn-white float-right m-2">
			        <i class="fas fa-2x fa-close"></i>
				</button>
	
			</div>
			
		    <ul class="nav flex-column  ">
		

			    <li class="nav-item my-2">
					
				
			    <a href="index.jsp" class="nav-link text-dark font-italic" id="home">
					    <i class="fas fa-home text-dark mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Home</font>	
				    </a>
			    </li>
			    <li class="nav-item my-2">
				    <a href="" class="nav-link text-dark font-italic" id="Products">
					    <i class="fas fa-th text-danger mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Products</font>	
				    </a>
				</li>
				

			    <li class="nav-item my-2">
				    <a href="" class="nav-link text-dark font-italic" id="about">
					    <i class="fas fa-address-card text-info mr-2" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">About us</font>	
				    </a>
			    </li>
			    <li class="nav-item my-2">
				    <a href="" class="nav-link text-dark font-italic" id="contact">
					    <i class="fas fa-phone text-success mr-3" style="font-size:1.4em"></i><font color="black" style="font-size:1.4em">Contact us</font>	
				    </a>
			    </li>
		    </ul>
		</div>
		<div class="page-content active" id="content" style="min-height: 100vh;">
			<nav id="navbar">
				
				<div class="nav-item bg-white">
					<div class="media row m-0 align-items-center">
					
						<div class="col-lg-3 col-md-9 col-sm-9 col-9">
							<button id="sidebarCollapse" type="button" class="btn btn-white float-left  mr-2">
								<i class="fas fa-2x fa-bars"></i> 
							</button>
							<a class="btn p-0 m-0" href="index.jsp"><font color="gray" style="font-size:1.8em">Uni</font><font color="red" style="font-size:1.8em">Script</font></a>
							
						</div>
						
                        <div class="col-lg-1 col-md-3 col-sm-3 col-3 mb-4">
						
				            <a href="" class="btn btn-outline-white float-right m-0 p-0 border-0" id="wishlist">
					            <span class="badge badge-white text-danger mt-2 ml-4 py-0 px-1 Wishlist-items" style="font-size:1.1em;"></span><br><i class="fas fa-2x fa-star text-warning p-0 m-0" ></i>	
				            </a>
						</div>
						
						<form onsubmit="return false;" class="input-group col-lg-4 col-md-11 col-sm-11 col-11 m-auto p-0  d-flex justify-content-between">
							<div class="input-group-btn search-panel">
								<button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown">
									<span id="search_concept">all</span> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu cats" data-idcat="0" role="menu">
								
    
								</ul>
							</div>
							<input type="text" autocomplete="off" class="form-control searchbox" name="Product" placeholder="search..."  >
							<span class="input-group-btn">
								<button class="btn btn-outline-info searchbtn" type="submit"><i class="fas fa-search p-1"></i></button>
							</span>
                            <br/>
                            <table class="table table-hover bg-white" style="position: absolute;top: 100%;width: 100%;z-index:3" id="livesearch"></table>
						</form>
						
                        <div class="col-lg-1 col-md-4 col-sm-4 col-4 mb-4">
				            <a href="cart.jsp" class="btn btn-outline-white float-left m-0 p-0 border-0" id="cart">
					            <span class="badge badge-white text-danger mt-2 ml-4 py-0 px-1 cart-items" style="font-size:1.1em;"></span><br><i class="fas fa-2x fa-shopping-cart text-success p-0 m-0" ></i>	
				            </a>
			            </div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-6 p-0 ">
							<div class="media btn btn-outline-success d-flex align-items-left float-right m-2 p-0 rounded-pill" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<img src="" onerror="this.onerror=null; this.src='https:\/\/s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg'"  alt="..." width="55" class="m-0 p-0 rounded-circle img-thumbnail shadow-sm notif-img">
								<div class="media-body mx-3">
                                                                    <h4 class="m-0 " style="word-wrap: break-word;"><%out.println(username);%></h4>
									<i class="font-weight-light text-muted mb-0">admin</i>
									<div class="dropdown-menu dropdown-menu-right mt-2">
										<a href=""   class="dropdown-item" ><i class="fas fa-eye mr-2"></i> View profile</a>
										<a class="dropdown-item" href=""><i class="fas fa-edit mr-2 text-info"></i> Edit</a>
										<a href="" class="dropdown-item" id="logout" onclick="event.preventDefault();	document.getElementById('logout-form').submit();">
											<form id="logout-form" action="logout.jsp" method="POST" style="display: none;">
												
											</form>
											<i class="fas fa-sign-out-alt mr-2 text-danger"></i> Log out</a>
										</a>
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</nav>
			<div class="container-fluid bg-light p-5 all" style="min-height:100vh" >
			
<!---->
                            <p class="text-gray font-weight-bold text-uppercase px-3 small pt-4 mb-0">Catégories</p>
                            <ul class="nav flex-column bg-white mb-0 cats_list">

                            </ul>
			<div class="container-fluid py-4 rounded" id="products_list">
                
                        </div>
			</div>
            <div id="snackbar" class="rounded-pill"></div>
            <div id="snackbar2" class="rounded-pill"></div>
			<footer class="sticky-bottom bg-light">
				<center>&copy;2020 - UniScript TECH</center>
			</footer>
		</div>
            <script>
	$(document).ready(function(){
                
                fetch_cats();
                fetch_products(1);
                fetch_product_info(1);
                $(window).click(function() {
                    $('#sidebar, #content').addClass('active');
                });
                
                $('.searchbox').blur(function() {
                   $('#livesearch').html("");
                });
                $('.searchbox').focus(function() {
                   $('.searchbox').keyup();
                });

                $(document).on("click", '.catclick', function(event) { 
                       var name = $(this).text();
					   var id =  $(this).data('idcat'); 
					   $('.search-panel span#search_concept').text(name);
                       $('.cats').attr("data-idcat",id);
                    });
                                    function fetch_products(i)
                {	
                    $.ajax({
                            url:"getProduits",
                            type: "POST",
                            data:{id_cat:i},
                            success:function(data){
                                $("#products_list").html(data);
                            }
                    });
                }
                function fetch_cats()
                {	
                    $.ajax({
                            url:"getCats",
                            type: "POST",
                            async:"false",
                            success:function(data){
                                $(".cats_list").html(data);
                            }
                    });
                }
                function fetch_product_info(i)
                {	
                    $.ajax({
                            url:"getProduitInfo",
                            type: "POST",
                            data:{id_prod:i},
                            success:function(data){
                                $(".infos").html(data);
                            }
                    });
                }
                
				$(document).on('click', '.getProducts', function(){
                    var cat_id = $(this).data('catid');
                    $('.getProducts').each(function() {
                            if($(this).data('catid')==cat_id)
                            {
                                $( this ).addClass(" bg-light" );
                            }
                            else
                            {
                                $( this ).removeClass( " bg-light" );
                            }
                    });
                    fetch_products(cat_id);
                });
                $(document).on('click', '.getProductInfo', function(){
                    var prod_id = $(this).data('prodid');
                    $('.getProductInfo').each(function() {
                            if($(this).data('prodid')==prod_id)
                            {
                                 $( this ).removeClass( " bg-light" );
                                 $( this ).addClass(" bg-dark text-light" );
                               
                            }
                            else
                            {                               
                                $( this ).removeClass( " bg-dark text-light" );
                                $( this ).addClass( " bg-light" );
                            }
                    });
                    fetch_product_info(prod_id);
                });
	});
			$(function() {
			  $('#sidebarCollapse').on('click', function() {
			  event.stopPropagation();
				$('#sidebar, #content').removeClass('active');
			  });
			  $('#sidebarCollapse2').on('click', function() {
				$('#sidebar, #content').addClass('active');
			  });
			});  

 function delay(callback, ms) {
  var timer = 0;
  return function() {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}
function add_to_cart(i)
                {	
                    $.ajax({
                            url:"addCart",
                            type: "POST",
                            data:{id_prod:i},
                            success:function(data){
                                //$('.cart-items').text(data.items);
                                $('#snackbar').html(data);
                                notify();
                            }
                    });
                }
 function notify() {
         var x = document.getElementById('snackbar');
         x.className = "rounded-pill show";
        setTimeout(function(){
                   x.className = x.className.replace("show", ""); }, 3000);
    }
    function notify2() {
         var x = document.getElementById('snackbar2');
         x.className = "rounded-pill show";
        setTimeout(function(){
                   x.className = x.className.replace("show", ""); }, 3000);
    }
    $('[data-toggle="collapse"]').click(function() {
        $(this).toggleClass( "active" );
        if ($(this).hasClass("active")) {
          $(this).text("Hide");
        } else {
          $(this).text("Show");
        }
      });
    var Mode = getCookie("DarkMode");
                    if (Mode!="" && Mode=="ON")
		            {
			            $('.theme').prop('checked', true);
        	            activate_dark_theme();
		            }
                    else
                    {
			            $('.theme').prop('checked', false);
			            activate_light_theme();
		            }
                    function activate_dark_theme()
	                {
                         $('.lightbtn').removeClass('active');
                         $('.darkbtn').addClass('active');
		                var css = 
		                'html,img {filter: invert(95%) hue-rotate(180deg)}',
		                head = document.getElementsByTagName('head')[0],
		                style = document.createElement('style');
		                style.type = 'text/css';
		                if (style.styleSheet){
		                style.styleSheet.cssText = css;
		                } 
		                else {
		                style.appendChild(document.createTextNode(css));
		                }
		                head.replaceChild(style,document.getElementsByTagName('style')[0]);
                        setCookie("DarkMode","ON","5");
	                }
	                function activate_light_theme()
	                {
                        $('.darkbtn').removeClass('active');
                         $('.lightbtn').addClass('active');
		                var css = 'html {-webkit-filter: invert(0%);' +
		                '-moz-filter: invert(0%);' + 
		                '-o-filter: invert(0%);' + 
		                '-ms-filter: invert(0%); }',
		                head = document.getElementsByTagName('head')[0],
		                style = document.createElement('style');
		                style.type = 'text/css';
		                if (style.styleSheet){
		                style.styleSheet.cssText = css;
		                } 
		                else {
		                style.appendChild(document.createTextNode(css));
		                }
		                head.replaceChild(style,document.getElementsByTagName('style')[0]);	
                        setCookie("DarkMode","OFF","5");
                    }
                    
		            function setCookie(cname, cvalue, exdays) {
		              var d = new Date();
		              d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		              var expires = "expires="+d.toUTCString();
		              document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		            }
		            function getCookie(cname) {
		                  var name = cname + "=";
		                  var decodedCookie = decodeURIComponent(document.cookie);
		                  var ca = decodedCookie.split(';');
		                  for(var i = 0; i <ca.length; i++) {
			                var c = ca[i];
			                while (c.charAt(0) == ' ') {
			                  c = c.substring(1);
			                }
			                if (c.indexOf(name) == 0) {
			                  return c.substring(name.length, c.length);
			                }
		                  }
		                  return "";
					 }
					 function showUserForm(){
						$('#f1').show();
						$('#f2').hide();
						$('#f3').hide();
					 }
					 function showEmployeesForm(){
						$('#f1').hide();
						$('#f2').show();
						$('#f3').show();
					}

		</script>
	</body>
</html>