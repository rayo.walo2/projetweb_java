<%-- 
    Document   : result
    Created on : 2020-04-24, 21:16:59
    Author     : rayow
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>File Upload Example in JSP and Servlet - Java web application</title>
    </head>
  
    <body> 
        <div id="result">
            <h3>${requestScope["message"]}</h3>
        </div>
       
    </body>
</html>