<%-- 
    Document   : logout
    Created on : 2020-04-23, 05:59:05
    Author     : rayow
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center><h1>waiting for logout...</h1></center>
    <%
    session.invalidate();
    String redirectURL = "index.jsp";
    response.sendRedirect(redirectURL);
    %>
    </body>
</html>
